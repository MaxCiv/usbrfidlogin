/*
 * GccApplication1.c
 *
 * Created: 03.04.2017 22:33:25
 * Author : MaxCiv
 */ 

// DDR - ����/�����, PIN - �����, PORT - ����
// �� DDR 0 - ����, 1 - �����

#include <stdlib.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <util/delay.h>			// for _delay_ms()
#include "oddebug.h"
#include "UsbKeyboardC.h"
#include "MFRC522_M.h"

#define BUTTON_DDR DDRD
#define BUTTON_PORT PORTD
#define PIN_BUTTON PIND7	// 7

#define WAIT_MS_T 40		// ����� �������� ��� ������� wait_Tms

#define CARD_ID_00 82	// ����� �����
#define CARD_ID_01 53
#define CARD_ID_02 104
#define CARD_ID_03 133	
#define CARD_ID_04 138	

int lastState = 0,  // ���������� ��������� ������: 0 - �� ������, 1 - ������
	logOn = 0;		// �������� �� ����: 0 - ���, 1 - ��

// ������� �������� � USB �����������
void wait_Tms(int n) {
	for (int i=1;i<=n;i++)
	{
		_delay_ms(WAIT_MS_T);
		usbPoll();
	}
}

int main(void)
{
	MFRC522_Init();
	uchar card_num[5];
	_delay_ms(40);
	
	initUsbKeyboard();		// ������������� � ����������� USB ����������
	
	//BUTTON_DDR &= ~(1 << PIN_BUTTON);	// ���� � ������� ����������� �� ����
	
	// ������� ���� //
    while (1) 
    {
		usbPoll();	// �������� ������ USB ����������
		
		if ( MFRC522_Request( PICC_REQIDL, card_num ) == MI_OK ) {
			if ( (MFRC522_Anticoll(card_num) == MI_OK) && (lastState == 0) && (card_num[0] == CARD_ID_00) && (card_num[1] == CARD_ID_01) && (card_num[2] == CARD_ID_02) && (card_num[3] == CARD_ID_03) && (card_num[4] == CARD_ID_04) ) {
				if (logOn == 0) {       // ���� ���� �� ��������
					sendKeyStroke(KEY_ENTER);
					
					wait_Tms(5);
					
					//sendKeyStrokeMod(KEY_9, MOD_CONTROL_LEFT);   // ���������� ����.���� ���������� ������ Ctrl+9
					sendKeyStroke(KEY_1);       // ������ ������
					sendKeyStroke(KEY_1);
					sendKeyStroke(KEY_1);
					sendKeyStroke(KEY_1);
					
					wait_Tms(2);
					
					sendKeyStroke(KEY_ENTER);   // ����������� ����
					logOn = 1;         // ���� ��������
				}
				else      // ���� ���� ��������
				{
					sendKeyStrokeMod(KEY_L, MOD_GUI_LEFT);   // ��������� ������� ���������� ������ Win+L
					logOn = 0;          // ������� �������������
				}
				
				lastState = 1;
			}
		} else if (( MFRC522_Request( PICC_REQIDL, card_num ) != MI_OK ) && ( MFRC522_Anticoll( card_num ) != MI_OK ) && (lastState == 1)) {
			lastState = 0;
		}
		
		wait_Tms(1);
		/*
		byte b;
		b = PIND;
		// ���������, ������ �� ������ � ����� �� ���������� ���������
		if( (((1 << PIN_BUTTON) & b) == 0b10000000) && (lastState == 0) ) {   // ���� ������ ������ � �� ����� ���� ������
			if (logOn == 0) {       // ���� ���� �� ��������
				sendKeyStroke(KEY_ENTER);
				
				for (int i=1;i<=5;i++)
				{
					_delay_ms(40);
					usbPoll();
				}
				
				//sendKeyStrokeMod(KEY_9, MOD_CONTROL_LEFT);   // ���������� ����.���� ���������� ������ Ctrl+9
				sendKeyStroke(KEY_1);       // ������ ������
				sendKeyStroke(KEY_1);
				sendKeyStroke(KEY_1);
				sendKeyStroke(KEY_1);
				
				for (int i=1;i<=2;i++)
				{
					_delay_ms(40);
					usbPoll();
				}
				
				sendKeyStroke(KEY_ENTER);   // ����������� ����
				logOn = 1;         // ���� ��������
			}
			else      // ���� ���� ��������
			{
				//sendKeyStrokeMod(KEY_L, MOD_SHIFT_LEFT);
				sendKeyStrokeMod(KEY_L, MOD_GUI_LEFT);   // ��������� ������� ���������� ������ Win+L
				logOn = 0;          // ������� �������������
			}
			lastState = 1; // ���������� ��������� - ������ ������
			_delay_ms(1);
			// ���� ������ ������
		} else if( (((1 << PIN_BUTTON) & b) == 0b00000000) && (lastState == 1) ) {
			lastState = 0; // ���������� ��������� - ������ ������
			_delay_ms(1);
		}*/
		
    }
}

//#include <avr/io.h>
//#include <avr/wdt.h>
//#include <avr/interrupt.h>  /* for sei() 
/*
uchar i = 0;
while(--i){             // ����� > 250 ms
	_delay_ms(1);
}
*/
//#include <util/delay.h>     /* for _delay_ms() */
//
//#include <avr/pgmspace.h>   /* required by usbdrv.h */
//#include "usbdrv.h"
//#include "oddebug.h"        /* This is also an example for using debug macros */
//
///* ------------------------------------------------------------------------- */
///* ----------------------------- USB interface ----------------------------- */
///* ------------------------------------------------------------------------- */
//
//PROGMEM const char usbHidReportDescriptor[52] = { /* USB report descriptor, size must match usbconfig.h */
    //0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
    //0x09, 0x02,                    // USAGE (Mouse)
    //0xa1, 0x01,                    // COLLECTION (Application)
    //0x09, 0x01,                    //   USAGE (Pointer)
    //0xA1, 0x00,                    //   COLLECTION (Physical)
    //0x05, 0x09,                    //     USAGE_PAGE (Button)
    //0x19, 0x01,                    //     USAGE_MINIMUM
    //0x29, 0x03,                    //     USAGE_MAXIMUM
    //0x15, 0x00,                    //     LOGICAL_MINIMUM (0)
    //0x25, 0x01,                    //     LOGICAL_MAXIMUM (1)
    //0x95, 0x03,                    //     REPORT_COUNT (3)
    //0x75, 0x01,                    //     REPORT_SIZE (1)
    //0x81, 0x02,                    //     INPUT (Data,Var,Abs)
    //0x95, 0x01,                    //     REPORT_COUNT (1)
    //0x75, 0x05,                    //     REPORT_SIZE (5)
    //0x81, 0x03,                    //     INPUT (Const,Var,Abs)
    //0x05, 0x01,                    //     USAGE_PAGE (Generic Desktop)
    //0x09, 0x30,                    //     USAGE (X)
    //0x09, 0x31,                    //     USAGE (Y)
    //0x09, 0x38,                    //     USAGE (Wheel)
    //0x15, 0x81,                    //     LOGICAL_MINIMUM (-127)
    //0x25, 0x7F,                    //     LOGICAL_MAXIMUM (127)
    //0x75, 0x08,                    //     REPORT_SIZE (8)
    //0x95, 0x03,                    //     REPORT_COUNT (3)
    //0x81, 0x06,                    //     INPUT (Data,Var,Rel)
    //0xC0,                          //   END_COLLECTION
    //0xC0,                          // END COLLECTION
//};
///* This is the same report descriptor as seen in a Logitech mouse. The data
 //* described by this descriptor consists of 4 bytes:
 //*      .  .  .  .  . B2 B1 B0 .... one byte with mouse button states
 //*     X7 X6 X5 X4 X3 X2 X1 X0 .... 8 bit signed relative coordinate x
 //*     Y7 Y6 Y5 Y4 Y3 Y2 Y1 Y0 .... 8 bit signed relative coordinate y
 //*     W7 W6 W5 W4 W3 W2 W1 W0 .... 8 bit signed relative coordinate wheel
 //*/
//typedef struct{
    //uchar   buttonMask;
    //char    dx;
    //char    dy;
    //char    dWheel;
//}report_t;
//
//static report_t reportBuffer;
//static int      sinus = 7 << 6, cosinus = 0;
//static uchar    idleRate;   /* repeat rate for keyboards, never used for mice */
//
//
///* The following function advances sin/cos by a fixed angle
 //* and stores the difference to the previous coordinates in the report
 //* descriptor.
 //* The algorithm is the simulation of a second order differential equation.
 //*/
//static void advanceCircleByFixedAngle(void)
//{
//char    d;
//
//#define DIVIDE_BY_64(val)  (val + (val > 0 ? 32 : -32)) >> 6    /* rounding divide */
    //reportBuffer.dx = d = DIVIDE_BY_64(cosinus);
    //sinus += d;
    //reportBuffer.dy = d = DIVIDE_BY_64(sinus);
    //cosinus -= d;
//}
//
///* ------------------------------------------------------------------------- */
//
//usbMsgLen_t usbFunctionSetup(uchar data[8])
//{
//usbRequest_t    *rq = (void *)data;
//
    ///* The following requests are never used. But since they are required by
     //* the specification, we implement them in this example.
     //*/
    //if((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS){    /* class request type */
        //DBG1(0x50, &rq->bRequest, 1);   /* debug output: print our request */
        //if(rq->bRequest == USBRQ_HID_GET_REPORT){  /* wValue: ReportType (highbyte), ReportID (lowbyte) */
            ///* we only have one report type, so don't look at wValue */
            //usbMsgPtr = (void *)&reportBuffer;
            //return sizeof(reportBuffer);
        //}else if(rq->bRequest == USBRQ_HID_GET_IDLE){
            //usbMsgPtr = &idleRate;
            //return 1;
        //}else if(rq->bRequest == USBRQ_HID_SET_IDLE){
            //idleRate = rq->wValue.bytes[1];
        //}
    //}else{
        ///* no vendor specific requests implemented */
    //}
    //return 0;   /* default for not implemented requests: return no data back to host */
//}
//
///* ------------------------------------------------------------------------- */
//
//int __attribute__((noreturn)) main(void)
//{
	//uchar   i;
	//usbInit();
	//usbDeviceDisconnect();  /* enforce re-enumeration, do this while interrupts are disabled! */
	//i = 0;
	//while(--i){             /* fake USB disconnect for > 250 ms */
		//_delay_ms(1);
	//}
	//usbDeviceConnect();
	//DDRC=0b00000011;
	//sei();
	//for(;;){                /* main event loop */
		//usbPoll();
		//if(usbInterruptIsReady()){
			//PORTC=0b00000011;
			///* called after every poll of the interrupt endpoint */
			//advanceCircleByFixedAngle();
			//usbSetInterrupt((void *)&reportBuffer, sizeof(reportBuffer));
		//}
		//PORTC=0b00000000;
	//}
//}